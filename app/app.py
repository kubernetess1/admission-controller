from flask import Flask, request, jsonify
import base64
import jsonpatch

app = Flask(__name__)

@app.route('/validate/deployments', methods=['POST'])
def deployment_webhook():
    request_info = request.get_json()
    uid = request_info["request"]["uid"]
    if request_info["request"]["object"]["metadata"]["labels"].get("allow"):
        return admission_response(True, "Allow label exists", uid)    
    
    return admission_response(False, "Not allowed without allow label", uid)

def admission_response(allowed, message, uid):    
    return jsonify(
        {
            "apiVersion": "admission.k8s.io/v1",
            "kind": "AdmissionReview",
            "response": {
                "uid": uid, 
                "allowed": allowed, 
                "status": {"message": message}
            }
        }
    )

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/mutate/deployments', methods=['POST'])
def deployment_webhook_mutate():
    return admission_response_patch(True, "Adding allow label", json_patch = jsonpatch.JsonPatch(
        [{
            "op": "add", 
            "path": "/metadata/labels/allow", 
            "value": "nope"
        }]))

def admission_response_patch(allowed, message, json_patch):
    utf8_encoded = json_patch.to_string().encode("utf-8")
    base64_patch = base64.b64encode(utf8_encoded).decode("utf-8")
    request_info = request.get_json()
    uid = request_info["request"]["uid"]
    return jsonify(
        {
            "apiVersion": "admission.k8s.io/v1",
            "kind": "AdmissionReview",        
            "response": {
                "uid": uid,    
                "allowed": allowed,
                "status": {
                    "message": message
                },
                "patchType": "JSONPatch",
                "patch": base64_patch
            }
        })

if __name__ == '__main__':
    app.run(debug = True, host='0.0.0.0', port=5000, ssl_context=("/flask/tls.crt", "/flask/tls.key"))