## Scripts

```bash
export DOMAIN=webhook                               #name of the webhook service
export NAMESPACE=default                               #namespace of the webhook svc
export VALIDITY_DAYS=365                                       #validity of the certificates
export KUBECONFIG_PATH_MASTER_NODE=/etc/kubernetes/webhook-pki #hostPath for kube-api server
```

## Generate certificates

```bash
openssl genrsa -out ${DOMAIN}_CA.key 4096 &>/dev/null
openssl req -new -x509 -days "${VALIDITY_DAYS}" -key ${DOMAIN}_CA.key -subj "/CN=${DOMAIN}.${NAMESPACE}.svc" -out ${DOMAIN}_CA.crt
openssl req -newkey rsa:4096 -nodes -keyout ${DOMAIN}.key -subj "/CN=${DOMAIN}.${NAMESPACE}.svc" -out ${DOMAIN}.csr
openssl x509 -req -extfile <(printf "subjectAltName=DNS:${DOMAIN}.${NAMESPACE}.svc,DNS:${DOMAIN}.${NAMESPACE}.svc.cluster.local") -days "${VALIDITY_DAYS}" -in ${DOMAIN}.csr -CA ${DOMAIN}_CA.crt -CAkey ${DOMAIN}_CA.key -CAcreateserial -out ${DOMAIN}.crt &>/dev/null
```

## Read from certificate ans encode to base64
```bash
CA_BUNDLE=$(cat cert2/webhook_CA.crt | base64 | tr -d '\n');

sed -e 's@${CA_BUNDLE}@'"$CA_BUNDLE"'@g' <"templates/mutatingwebhookwonfiguration.yaml" > MutatingWebhookConfiguration.yaml

sed -e 's@${CA_BUNDLE}@'"$CA_BUNDLE"'@g' <"templates/validationwebhook.yaml" > ValidatingWebhookConfiguration.yaml
```


## Execute kubernetes manifest
```bash
k apply -f manifests/deployment_service.yaml
k apply -f manifests/MutatingWebhookConfiguration.yaml
```

## Check if pod fail or not
```bash
k apply -f manifests/fail_pod.yaml
```